import Card from "./Card.js";
import Game from "./Game.js";
import SpeedRate from "./SpeedRate.js";

// Отвечает является ли карта уткой.
function isDuck(card) {
    return card && card.quacks && card.swims;
}

// Отвечает является ли карта собакой.
function isDog(card) {
    return card instanceof Dog;
}

// Дает описание существа по схожести с утками и собаками
function getCreatureDescription(card) {
    if (isDuck(card) && isDog(card)) {
        return 'Утка-Собака';
    }
    if (isDuck(card)) {
        return 'Утка';
    }
    if (isDog(card)) {
        return 'Собака';
    }
    return 'Существо';
}

class Creature extends Card {
    constructor(name, maxPower, image) {
        super(name, maxPower, image);
    }
    getDescriptions() {
        return [
            getCreatureDescription(this),
            super.getDescriptions(),
        ];
    }
}

// Основа для утки.
class Duck extends Creature {
    constructor(name = 'Мирная утка', maxPower = 2, image) {
        super(name, maxPower, image);
    }
    quacks = () => { console.log('quack') };
    swims = () => { console.log('float: both;') };
}

// Основа для собаки.
class Dog extends Creature {
    constructor(name = 'Пес-бандит', maxPower = 3, image) {
        super(name, maxPower, image);
    }
    swims = () => { console.log('float: none;') };
}

class Lad extends Dog {
    constructor(name = 'Браток', maxPower = 2, image) {
        super(name, maxPower, image);
    }
    static getInGameCount() {
        return this.inGameCount || 0;
    }
    static setInGameCount(value) {
        this.inGameCount = value;
    }
    doAfterComingIntoPlay(gameContext, continuation) {
        super.doAfterComingIntoPlay(gameContext, continuation);
        Lad.setInGameCount(Lad.getInGameCount() + 1);
    }
    doBeforeRemoving(continuation) {
        super.doBeforeRemoving(continuation);
        Lad.setInGameCount(Lad.getInGameCount() - 1);
    }
    static getBonus() {
        const ladsCount = this.getInGameCount();
        return (ladsCount * (ladsCount + 1)) / 2;
    }
    modifyDealedDamageToCreature(value, toCard, gameContext, continuation) {
        const bonus = Lad.getBonus(value);
        super.modifyDealedDamageToCreature(
            value + bonus,
            toCard,
            gameContext,
            continuation
        );
    }
    modifyTakenDamage(value, fromCard, gameContext, continuation) {
        const bonus = Lad.getBonus(value);
        super.modifyTakenDamage(value - bonus,
            fromCard,
            gameContext,
            continuation
        );
    }
    getDescriptions() {
        let description = super.getDescriptions();
        const isModified = Lad.prototype.hasOwnProperty("modifyDealedDamageToCreature") ||
            Lad.prototype.hasOwnProperty("modifyTakenDamage");
        if (isModified) {
            description[0] = "Чем их больше, тем они сильнее";
        }
        return description;
    }
}

// Колода Шерифа, нижнего игрока.
const seriffStartDeck = [
    new Duck(),
    new Duck(),
    new Duck(),
];

// Колода Бандита, верхнего игрока.
const banditStartDeck = [
    new Lad(),
    new Lad(),
];


// Создание игры.
const game = new Game(seriffStartDeck, banditStartDeck);

// Глобальный объект, позволяющий управлять скоростью всех анимаций.
SpeedRate.set(1);

// Запуск игры.
game.play(false, (winner) => {
    alert('Победил ' + winner.name);
});
